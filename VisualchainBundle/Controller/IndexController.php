<?php


namespace VisualChain\VisualchainBundle\Controller;


use BaseApp\BaseappBundle\Entity\Alert;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class IndexController extends AbstractController
{
    public function index()
    {
        return $this->render('@Visualchain/index/index.html.twig');
    }
}