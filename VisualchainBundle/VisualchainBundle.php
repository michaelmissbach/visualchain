<?php

namespace VisualChain\VisualchainBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class VisualchainBundle
 * @package VisualChain\VisualchainBundle
 */
class VisualchainBundle extends Bundle
{
    /**
     * @param ContainerBuilder $container
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);


    }
}
